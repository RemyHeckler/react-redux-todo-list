const editTask = changedTask => ({
  type: 'CHANGE_TASK',
  payload: changedTask,
});

export default editTask;
