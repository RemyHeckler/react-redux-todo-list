const findTask = searchValue => ({
  type: 'FIND_TASK',
  payload: searchValue,
});

export default findTask;
