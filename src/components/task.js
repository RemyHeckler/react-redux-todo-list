import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Task extends Component {
  static propTypes = {
    data: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      deadline: PropTypes.string,
      assignee: PropTypes.string,
      description: PropTypes.string,
    }),
    onSaveChangeClick: PropTypes.func,
    onDeleteClick: PropTypes.func,
  }
  state = {
    id: '',
    name: '',
    deadline: '',
    assignee: '',
    description: '',
    showDescription: false,
    edit: false,
  }

  handleShowDescriptions = () => {
    this.setState({
      showDescription: !this.state.showDescription,
    });
  }

  handleChangeClick = (id, name, deadline, assignee, description) => {
    this.setState({
      id,
      name,
      deadline,
      assignee,
      description,
      edit: true,
    });
  }

  handleCancelClick = (action) => {
    this.setState({
      edit: false,
    });
    if (action === 'save') {
      const changedTask = {
        id: this.state.id,
        name: this.state.name,
        deadline: this.state.deadline,
        assignee: this.state.assignee,
        description: this.state.description,
      };
      this.props.onSaveChangeClick(changedTask);
    }
  }

  handleChangedInput = (key, event) => {
    this.setState({
      [key]: event.target.value,
    });
  }

  render() {
    const {
      id, name, deadline, assignee, description,
    } = this.props.data;
    const { onDeleteClick } = this.props;
    return (
      !this.state.edit ?
        <div className="flex justify-between flex-wrap border task">
          <div onClick={this.handleShowDescriptions} className="col-10 flex justify-between">
            <div className="col-2">{id}</div>
            <div className="col-3">{name}</div>
            <div className="col-2">{deadline}</div>
            <div className="col-3">{assignee}</div>
          </div>
          <div className="col-2">
            <button onClick={() => onDeleteClick(id)} className="inline-block">Delete</button>
            <button onClick={() => this.handleChangeClick(id, name, deadline, assignee, description)} className="inline-block">Edit</button>
          </div>
          {this.state.showDescription ? <div className="border-top col-12">{description}</div> : null}
        </div> :
        <div className="flex justify-between items-center edit">
          <div className="col-2">
            <p>Name</p>
            <input defaultValue={name} onChange={event => this.handleChangedInput('name', event)} />
          </div>
          <div className="col-2">
            <p>Deadline</p>
            <input type="date" defaultValue={deadline} onChange={event => this.handleChangedInput('deadline', event)} />
          </div>
          <div className="col-2">
            <p>Assigne</p>
            <input defaultValue={assignee} onChange={event => this.handleChangedInput('assignee', event)} />
          </div>
          <div className="col-2">
            <p>Description</p>
            <textarea defaultValue={description} onChange={event => this.handleChangedInput('description', event)} />
          </div>
          <div className="col-2">
            <button
              onClick={() => this.handleCancelClick('save')}
              disabled={
                !(this.state.name &&
                  this.state.description &&
                  this.state.assignee &&
                  this.state.deadline
                )
              }
              className="inline-block"
            >
            Save
            </button>
            <button onClick={this.handleCancelClick} className="inline-block">Cancel</button>
          </div>
        </div>
    );
  }
}

export default Task;
