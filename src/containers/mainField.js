import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import addTask from '../actions/addTask';
import findTask from '../actions/findTask';

class MainField extends Component {
  static propTypes = {
    addTask: PropTypes.func,
    findTask: PropTypes.func,
  }

  state = {
    name: '',
    description: '',
    assignee: '',
    deadline: '',
    searchValue: '',
    selectAdd: false,
  }

  handleChangeInput = (key, event) => {
    this.setState({
      [key]: event.target.value,
    });
  }

  addTask = () => {
    const id = Date.now();
    const task = {
      id,
      name: this.state.name,
      description: this.state.description,
      assignee: this.state.assignee,
      deadline: this.state.deadline,
    };
    this.props.addTask(task);
    this.setState({
      selectAdd: false,
    });
  }

  handleSelectAdd = () => {
    this.setState({
      name: '',
      description: '',
      assignee: '',
      deadline: '',
      selectAdd: !this.state.selectAdd,
    });
  }

  render() {
    return (
      <div className="flex flex-column m1">
        {this.state.selectAdd ?
          <div className="flex justify-start col-12">
            <input
              className="col-1 m1"
              type="text"
              placeholder="Task Name"
              onChange={event => this.handleChangeInput('name', event)}
            />
            <textarea
              className="col-1 m1"
              rows="1"
              type="text"
              placeholder="Description"
              onChange={event => this.handleChangeInput('description', event)}
            />
            <input
              className="col-1 m1"
              type="text"
              placeholder="Assignee Name"
              onChange={event => this.handleChangeInput('assignee', event)}
            />
            <input
              className="col-1 m1"
              type="date"
              onChange={event => this.handleChangeInput('deadline', event)}
            />
            <button
              className="col-1 m1"
              onClick={this.addTask}
              disabled={
                !(this.state.name &&
                  this.state.description &&
                  this.state.assignee &&
                  this.state.deadline
                )
              }
            >
              Add
            </button>
            <button onClick={this.handleSelectAdd} className="m1 col-1">Cancel</button>
          </div> :
          <div>
            <button onClick={this.handleSelectAdd} className="col-1 m1 center">Add Task</button>
          </div>
        }
        <div>
          <input placeholder="Input Assignee's Name" onChange={event => this.handleChangeInput('searchValue', event)} />
          <button onClick={() => this.props.findTask(this.state.searchValue)}>Find Task</button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    taskStore: state,
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    addTask,
    findTask,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(MainField);
