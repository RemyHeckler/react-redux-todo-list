import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import deleteTask from '../actions/deleteTask';
import editTask from '../actions/editTask';
import Task from '../components/task';

class TaskList extends Component {
  static propTypes = {
    onChangeTask: PropTypes.func,
    deleteTask: PropTypes.func,
    editTask: PropTypes.func,
    taskStore: PropTypes.array,
  }

  handleChangeTask(changedTask) {
    this.props.onChangeTask(changedTask);
  }

  render() {
    return (
      <div>
        {this.props.taskStore.map(task => (
          <Task
            key={task.id}
            data={task}
            onDeleteClick={id => this.props.deleteTask(id)}
            onSaveChangeClick={changedTask => this.props.editTask(changedTask)}
          />
          ))}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    taskStore: state.tasks.filter(task => task.assignee.includes(state.filterTasks)),
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    deleteTask,
    editTask,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(TaskList);
