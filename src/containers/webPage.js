import React from 'react';
import '../css/webPage.css';
import TaskList from './taskList';
import MainField from './mainField';


function WebPage() {
  return (
    <div className="center">
      <MainField />
      <div className="head flex justify-between">
        <div className="col-10 flex justify-between">
          <div className="col-2">#</div>
          <div className="col-3">Title</div>
          <div className="col-2">Deadline</div>
          <div className="col-3">Assignee</div>
        </div>
        <div className="col-2">Actions</div>
      </div>
      <div>
        <TaskList />
      </div>
    </div>
  );
}

export default WebPage;
