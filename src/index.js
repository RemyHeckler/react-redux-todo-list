/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';

import WebPage from './containers/webPage';
import reducers from './reducers';
import 'basscss/css/basscss.min.css';

import registerServiceWorker from './registerServiceWorker';
import './index.css';

const store = compose(applyMiddleware(createLogger()))(createStore)(reducers);

ReactDOM.render(
  <Provider store={store}>
    <WebPage />
  </Provider>,
  document.getElementById('root'),
);

registerServiceWorker();
