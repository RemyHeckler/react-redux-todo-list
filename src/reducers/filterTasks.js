const initialState = '';

const filterTasks = (state = initialState, action) => {
  if (action.type === 'FIND_TASK') {
    return action.payload;
  }
  return state;
};

export default filterTasks;
