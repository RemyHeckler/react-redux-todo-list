import { combineReducers } from 'redux';
import tasks from './tasks';
import filterTasks from './filterTasks';

const reducers = combineReducers({
  tasks,
  filterTasks,
});

export default reducers;
