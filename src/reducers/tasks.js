// TODO: Add Tasks related reducers here:
// We should be able to CRUD Task entities
// Every Task should include:
// 'task_id', 'task_title', 'task_description', 'deadline' and 'assignee' (some percon's name)

const initialState = [];

const tasks = (state = initialState, action) => {
  if (action.type === 'ADD_TASK') {
    return [
      ...state,
      action.payload,
    ];
  } else if (action.type === 'DELETE_TASK') {
    return state.filter(item => item.id !== action.payload);
  } else if (action.type === 'CHANGE_TASK') {
    return state.map(item => (item.id === action.payload.id ? action.payload : item));
  }
  return state;
};

export default tasks;
